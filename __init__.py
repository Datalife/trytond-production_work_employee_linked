# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .work import (WorkCenter, EmployeeWorkCenter, WorkCenterEmployeeRotate)


def register():
    Pool.register(
        WorkCenter,
        EmployeeWorkCenter,
        module='production_work_employee_linked', type_='model')
    Pool.register(
        WorkCenterEmployeeRotate,
        module='production_work_employee_linked', type_='wizard',
        depends=['production_work_employee_rotate'])
